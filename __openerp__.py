# -*- coding: utf-8 -*-


{
    'name': 'Maintenance management',
    'version': '1.1',
    'depends': [ 'product','hr','account'],
    'author': 'Helmi Dhaoui',
    'category': 'Uncategorized',
    'description': '''
       Maintenance management
    ''',
    'data': [
             'security/maintenance_security.xml',
             'security/ir.model.access.csv',
             
             'data/product_data.xml',
             'data/maintenance_cron.xml',
             
             'view/reception_view.xml',
             'view/maintenace_view.xml',
             'view/product_view.xml',
             'view/account_invoice_view.xml',
             'view/res_users_view.xml',
             
             'wizard/alerte_view.xml',
             
             
             ],
    'installable': True,

}
