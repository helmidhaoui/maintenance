# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class account_invoice(osv.Model):
    _inherit = 'account.invoice'
    _columns = {
        'maintenance_id':fields.many2one('maintenance.maintenance','maintenance'),
        'equipment_id':fields.many2one('product.template','Equipment'),
    }

