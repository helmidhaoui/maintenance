# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from datetime import datetime,date,timedelta
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _

class maintenance(osv.osv):
    _inherit = ['mail.thread']
    _name = 'maintenance.maintenance'
    
    def _build_name(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context):
            name = _('M [%s]%s') % (record.partner_id.name,record.equipment_id.name)
            res[record.id] = name
        return res
    
    
    def _amount_line_tax(self, cr, uid, line, context=None):
        val = 0.0
        line_obj = self.pool['maintenance.task']
        price = line_obj._calc_line_base_price(cr, uid, line, context=context)
        qty = line_obj._calc_line_quantity(cr, uid, line, context=context)
        product = self.pool.get('ir.model.data').get_object(cr, uid, 'maintenance', 'product_product_maintenace')
        for c in self.pool['account.tax'].compute_all(
                cr, uid, line.tax_id, price, qty,product,
                line.maintenance_id.partner_id)['taxes']:
            val += c.get('amount', 0.0)
        return val


    def _amount_all_wrapper(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = 0.0
            for line in order.service_ids:
                val1 += line.subtotal
                val += self._amount_line_tax(cr, uid, line, context=context)
            res[order.id]['amount_tax'] = val
            res[order.id]['amount_untaxed'] = val1
            res[order.id]['amount_total'] = val+val1
        return res
    
    def _calcul_cout(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context):
            res[record.id] = sum(line.qty * line.unit_price for line in record.service_ids)
        return res
    
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('maintenance.task').browse(cr, uid, ids, context=context):
            result[line.maintenance_id.id] = True
        return result.keys()
    
    def _customer_inv_count(self, cr, uid, ids, field_name, arg, context=None):
        res = dict(map(lambda x: (x, 0), ids))
        try:
            for affaire in self.browse(cr, uid, ids, context):
                res[affaire.id] = len(self.pool.get('account.invoice').search(cr, uid, [('maintenance_id', '=', affaire.id), ('type', '=', 'out_invoice')]))
        except:
            pass
        return res
    
    _columns = {
                'name': fields.function(_build_name, type='char', string='Name', store=True),
                'reception_id':fields.many2one('maintenance.reception', 'Reception'),
                'equipment_id':fields.many2one('product.template','Equipment',required=True),
                'partner_id':fields.many2one('res.partner','Partner',required=True),
                'date_in': fields.date('Date in'),
                'date_out': fields.date('Date out'),
                'sale_date': fields.date('Sale date'), 
                'note': fields.text('Description'),
                'service_ids':fields.one2many('maintenance.task', 'maintenance_id', 'Services'),
                'state':fields.selection([
                    ('draft', 'Draft'),
                    ('confirmed', 'Confirmed'),
                    ('done', 'Done'),
                     ], 'State', select=True, readonly=True),
                'maintenance_cost': fields.function(_calcul_cout, method=True, type='float', string='Maintenance Cost', store=True),
                'user_id':fields.many2one('res.users','triggered by'),
                'repairer_id':fields.many2one('hr.employee', 'Maintenance Manager'),
                'amount_untaxed': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                            store={
                                'maintenance.maintenance': (lambda self, cr, uid, ids, c={}: ids, ['piece_ids', 'service_ids'], 10),
                                'maintenance.task': (_get_order, ['unit_price', 'tax_id', 'discount', 'qty'], 10),
                            }, string='Untaxed Amount', multi='sums',),
                'amount_tax': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                            store={
                                'maintenance.maintenance': (lambda self, cr, uid, ids, c={}: ids, ['piece_ids', 'service_ids'], 10),
                                'maintenance.task': (_get_order, ['unit_price', 'tax_id', 'discount', 'qty'], 10),
                            }, string='Taxes', multi='sums', help='The tax amount.'),
                'amount_total': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                            store={
                                'maintenance.maintenance': (lambda self, cr, uid, ids, c={}: ids, ['piece_ids', 'service_ids'], 10),
                                'maintenance.task': (_get_order, ['unit_price', 'tax_id', 'discount', 'qty'], 10),
                            }, string='Total', multi='sums', help='The total amount.'),
                'guarantee':fields.float('Guarantee'),
                'customer_inv_count': fields.function(_customer_inv_count, string='# of Invoices', type='integer'),
                'tasks_done':fields.boolean('Tasks Done'),
                'product_in_guarantee':fields.boolean('Product in guarantee'), 
                }
    _defaults = {  
        'state': 'draft',
        'date_in': lambda *a:datetime.now().strftime('%Y-%m-%d') ,
        'user_id':lambda self, cr, uid, ctx = None: uid
        }
    
    def _get_act_window_dict(self, cr, uid, name, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        result = mod_obj.xmlid_to_res_id(cr, uid, name, raise_if_not_found=True)
        result = act_obj.read(cr, uid, [result], context=context)[0]
        return result
    
    def action_view_customer_inv(self, cr, uid, ids, context=None):
        invoice_ids = self.pool.get('account.invoice').search(cr, uid, [('maintenance_id', '=', ids[0]), ('type', '=', 'out_invoice')])
        result = self._get_act_window_dict(cr, uid, 'account.action_invoice_tree1', context=context)
        if len(ids) == 1 and len(invoice_ids) == 1:
            result['domain'] = [('id', 'in', invoice_ids)]
            result['context'] = {
                         'default_maintenance_id': ids[0],
                         'search_default_maintenance_id': ids[0],
                         }
        else:
            result['domain'] = [('id', 'in', invoice_ids)]
            result['context'] = {
                                 'search_default_maintenance_id': ids[0],
                                'default_maintenance_id': ids[0],
                                }
        return result
    
        
    def create(self, cr, uid, vals, context=None):
        record_id = super(maintenance, self).create(cr, uid, vals, context=context)
        record = self.browse(cr, uid, record_id)
        if record.reception_id and record.reception_id.state == 'confirmed':
            record.reception_id.state = 'done'
            record.reception_id.maintenance_id = record_id
        return record_id
    
    def dummy(self, cr, uid, ids, context=None):
        return True
    
    def action_confirmed(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'confirmed'}, context=context)
    
    def action_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)
    
    def _choose_account_from_po_line(self, cr, uid, product_id, context=None):
        fiscal_obj = self.pool.get('account.fiscal.position')
        property_obj = self.pool.get('ir.property')
        if product_id:
            acc_id = product_id.property_account_expense.id
            if not acc_id:
                acc_id = product_id.categ_id.property_account_expense_categ.id
            if not acc_id:
                raise osv.except_osv(_('Error!'), _("Define an expense account for this product: '%s' (id:%d).") % (product_id.name, product_id.id,))
        else:
            acc_id = property_obj.get(cr, uid, 'property_account_expense_categ', 'product.category', context=context).id
        fpos = False
        return fiscal_obj.map_account(cr, uid, fpos, acc_id)
    
    def _prepare_inv_line(self, cr, uid, account_id, service, invoice,product, context=None):
        return {
            'name': service.name,
            'invoice_id':invoice,
            'account_id': account_id,
            'price_unit': service.unit_price or 0.0,
            'invoice_line_tax_id': [(6, 0, [x.id for x in service.tax_id])],
            'discount':service.discount,
            'quantity': service.qty,
            'product_id': product.id or False,
            'uos_id': product.uom_id.id or False,
        }
    
    def action_done(self, cr, uid, ids, context=None):
        record = self.browse(cr, uid, ids[0])
        self.pool.get('maintenance.task').write(cr, uid, record.service_ids.ids, {'state': 'done'}, context=context)
        return self.write(cr, uid, ids, {'state': 'done'}, context=context)
    
    def get_diff(self,sale_date,year,month):
        if sale_date:
            sale_date=datetime.strptime(sale_date, "%Y-%m-%d")
            date_end_guarantee=sale_date+timedelta(days=year*365+month*30)
            return datetime.strptime(str(date_end_guarantee), "%Y-%m-%d %H:%M:%S")>=datetime.strptime(str(date.today()), "%Y-%m-%d")   
    
    def create_invoice(self, cr, uid, ids, context=None):
        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')
        record = self.browse(cr, uid, ids[0])
        diff=self.get_diff(record.sale_date,record.equipment_id.year_guarantee,record.equipment_id.month_guarantee)
        if diff==False:
            raise osv.except_osv(_('Error!'), _(u"This equipment is in guarantee period"))
        else:
            vals = {
                    'equipment_id':record.equipment_id.id,
                    'account_id':record.partner_id.property_account_receivable.id,
                    'partner_id':record.partner_id.id,
                    'maintenance_id':record.id,
                  }
            invoice = inv_obj.create(cr, uid, vals)
            product = self.pool.get('ir.model.data').get_object(cr, uid, 'maintenance', 'product_product_maintenace')
            for service in record.service_ids:
                acc_id = self._choose_account_from_po_line(cr, uid, product, context=context)
                inv_line_data = self._prepare_inv_line(cr, uid, acc_id, service, invoice,product, context=context)
                inv_line_obj.create(cr, uid, inv_line_data, context=context)
                service.product_service_id=record.equipment_id.id
            self.write(cr, uid, ids, {'state': 'done'}, context=context)
            mod_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')
            result = mod_obj.get_object_reference(cr, uid, 'account', 'action_invoice_tree1')
            id = result and result[1] or False
            result = act_obj.read(cr, uid, [id], context=context)[0]
            res = mod_obj.get_object_reference(cr, uid, 'maintenance', 'account_invoice_form_view')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = invoice or False
            return result
    
    def onchange_equipment_id(self, cr, uid, ids, equipment_id,sale_date, context=None):
        if equipment_id:
            equipment=self.pool.get('product.template').browse(cr, uid, equipment_id, context)
            return {'value':{'product_in_guarantee':self.get_diff(sale_date,equipment.year_guarantee,equipment.month_guarantee) }}
        
class maintenance_task(osv.osv):
    _name = 'maintenance.task'
    
    def _calc_line_base_price(self, cr, uid, line, context=None):
        return line.unit_price * (1 - (line.discount or 0.0) / 100.0)

    def _calc_line_quantity(self, cr, uid, line, context=None):
        return line.qty

    def _calcul_cout(self, cr, uid, ids, field_name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        res = {}
        if context is None:
            context = {}
        product = self.pool.get('ir.model.data').get_object(cr, uid, 'maintenance', 'product_product_maintenace')
        for line in self.browse(cr, uid, ids, context=context):
            price = self._calc_line_base_price(cr, uid, line, context=context)
            qty = self._calc_line_quantity(cr, uid, line, context=context)
            taxes = tax_obj.compute_all(cr, uid, line.tax_id, price, qty,
                                        product,
                                        line.maintenance_id.partner_id)
            res[line.id] = taxes['total']
        return res
        
    _columns = {
                'maintenance_id':fields.many2one('maintenance.maintenance', 'Maintenance'),
                'product_service_id':fields.many2one('product.template', 'Equipment'),
                'name': fields.text('Work details',required=True),
                'repairer_id':fields.many2one('hr.employee', 'Assigned to',required=True),
                'date': fields.date('Date',required=True), 
                'qty': fields.float('Quantity'),
                'unit_price': fields.float('Price unit'),
                'discount': fields.float('Discount(%)'),
                'tax_id' : fields.many2many('account.tax', 'piece_tax_id', 'piece_id', 'tax_id', string='Taxes'),
                'subtotal': fields.function(_calcul_cout, method=True, type='float', string='Sous Total', store=True),
                'problem_notes':fields.text('Problem details'),
                'state':fields.selection([
                    ('draft', 'Draft'),
                    ('problem', 'Problem'),
                    ('done', 'Done'),
                     ], 'State', select=True, readonly=True),
                }
    
    _defaults = {  
        'qty': 1,
        'state':'draft',
        }
    
    def action_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)
    
    def action_problem(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'problem'}, context=context)
    
    def action_done(self, cr, uid, ids, context=None):
        res=self.write(cr, uid, ids, {'state': 'done'}, context=context)
        record = self.browse(cr,uid,ids[0],context)
        test=True
        for service in record.maintenance_id.service_ids:
            if service.state!='done':
                test=False
        if test:
            record.maintenance_id.tasks_done=True
        return res
        