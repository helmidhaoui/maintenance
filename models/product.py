# -*- coding: utf-8 -*-
from openerp.osv import osv,fields
from dateutil import relativedelta
from datetime import datetime

class product_template(osv.osv):
    _inherit = 'product.template'
    
    def _reception_count(self, cr, uid, ids, field_name, arg, context=None):
        res = dict(map(lambda x: (x, 0), ids))
        try:
            for product in self.browse(cr, uid, ids, context):
                res[product.id] = len(self.pool.get('maintenance.reception').search(cr, uid, [('equipment_id', '=', product.id)]))
        except:
            pass
        return res
    
    def _maintenance_count(self, cr, uid, ids, field_name, arg, context=None):
        res = dict(map(lambda x: (x, 0), ids))
        try:
            for product in self.browse(cr, uid, ids, context):
                res[product.id] = len(self.pool.get('maintenance.maintenance').search(cr, uid, [('equipment_id', '=', product.id)]))
        except:
            pass
        return res
    
    
    _columns = {
            'service_ids':fields.one2many('maintenance.task', 'product_service_id', 'Maintenance',readonly=True),
            'year_guarantee': fields.integer('year guarantee'),
            'month_guarantee': fields.integer('month guarantee'), 
            'reception_count': fields.function(_reception_count, string='# of reception', type='integer'),
            'maintenance_count': fields.function(_maintenance_count, string='# of maintenance', type='integer'),
                    }
    
    def _get_act_window_dict(self, cr, uid, name, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        result = mod_obj.xmlid_to_res_id(cr, uid, name, raise_if_not_found=True)
        result = act_obj.read(cr, uid, [result], context=context)[0]
        return result
    
    def action_view_reception(self, cr, uid, ids, context=None):
        reception_ids = self.pool.get('maintenance.reception').search(cr, uid, [('equipment_id', '=', ids[0])])
        result = self._get_act_window_dict(cr, uid, 'maintenance.action_maintenance_reception_tree_view', context=context)
        if len(ids) == 1 and len(reception_ids) == 1:
            result['domain'] = [('id', 'in', reception_ids)]
            result['context'] = {
                         'default_equipment_id': ids[0],
                         'search_default_equipment_id': ids[0],
                         }
        else:
            result['domain'] = [('id', 'in', reception_ids)]
            result['context'] = {
                                 'search_default_equipment_id': ids[0],
                                'default_equipment_id': ids[0],
                                }
        return result
    
    def action_view_maintenance(self, cr, uid, ids, context=None):
        maintenance_ids = self.pool.get('maintenance.maintenance').search(cr, uid, [('equipment_id', '=', ids[0])])
        result = self._get_act_window_dict(cr, uid, 'maintenance.action_maintenance_no_create_tree_view', context=context)
        if len(ids) == 1 and len(maintenance_ids) == 1:
            result['domain'] = [('id', 'in', maintenance_ids)]
            result['context'] = {
                         'default_equipment_id': ids[0],
                         'search_default_equipment_id': ids[0],
                         }
        else:
            result['domain'] = [('id', 'in', maintenance_ids)]
            result['context'] = {
                                 'search_default_equipment_id': ids[0],
                                'default_equipment_id': ids[0],
                                }
        return result

