# -*- coding: utf-8 -*-
from openerp.osv import osv,fields
from datetime import datetime,date,timedelta
from openerp.tools.translate import _

class maintenance_reception(osv.osv):
    _inherit = ['mail.thread']
    _name='maintenance.reception'
    
    def _build_name(self, cr, uid, ids, name, arg, context=None):
        res={}
        for record in self.browse(cr,uid,ids,context):
            name = _('R [%s]-%s') % (record.partner_id.name,record.equipment_id.name)
            res[record.id]=name
        return res
    
    _columns = {
                'name': fields.function(_build_name, type='char',string='Name', store=True),
                'maintenance_id':fields.many2one('maintenance.maintenance','Maintenance'),
                'equipment_id':fields.many2one('product.template','Equipment',required=True),
                'partner_id':fields.many2one('res.partner','Partner',required=True),
                'date_in': fields.date('Date in'),
                'sale_date': fields.date('Sale date'), 
                'note': fields.text('Required work'), 
                'state':fields.selection([
                    ('draft','Draft'),
                    ('confirmed','Confirmed'),
                    ('done','Done'),
                     ],    'State', select=True, readonly=True), 
                'user_id':fields.many2one('res.users','triggered by'),
                }
    _defaults = {  
        'state': 'draft',  
        'date_in': lambda *a:datetime.now().strftime('%Y-%m-%d'),
        'user_id':lambda self, cr, uid, ctx=None: uid
        }
    
    
    def action_confirmed(self,cr,uid,ids,context=None):
        return self.write(cr, uid, ids, {'state': 'confirmed'}, context=context)
    
    def action_draft(self,cr,uid,ids,context=None):
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)
    
    def get_diff(self,sale_date,year,month):
        if sale_date:
            sale_date=datetime.strptime(sale_date, "%Y-%m-%d")
            date_end_guarantee=sale_date+timedelta(days=year*365+month*30)
            return datetime.strptime(str(date_end_guarantee), "%Y-%m-%d %H:%M:%S")>=datetime.strptime(str(date.today()), "%Y-%m-%d")   
    
    def action_done(self,cr,uid,ids,context=None):
        record =self.browse(cr,uid,ids[0])
        diff=self.get_diff(record.sale_date,record.equipment_id.year_guarantee,record.equipment_id.month_guarantee)
        vals={
                'equipment_id':record.equipment_id.id,
                'product_in_guarantee':diff,
                'user_id':record.user_id.id,
                'partner_id':record.partner_id.id,
                'date_in':record.date_in,
                'reception_id':record.id,
                'sale_date':record.sale_date,
              }
        maintenance_id=self.pool.get('maintenance.maintenance').create(cr,uid,vals)
        self.write(cr, uid, ids, {'state': 'done','maintenance_id':maintenance_id}, context=context)
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        result = mod_obj.get_object_reference(cr, uid, 'maintenance', 'action_maintenance_tree_view')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        res = mod_obj.get_object_reference(cr, uid, 'maintenance', 'maintenance_form_view')
        result['views'] = [(res and res[1] or False, 'form')]
        result['res_id'] =  maintenance_id or False
        return result
    
