# -*- coding: utf-8 -*-


import openerp.tools
from openerp.osv import fields, osv
from openerp.tools.translate import _

from datetime import datetime, timedelta

date_format = '%Y-%m-%d'
#date_format = '%Y-%m-%d %H:%M:%S'

class res_users(osv.Model):
    _inherit = 'res.users'
    
  
    
    def send_project_alert_upcomming_tasks(self, cr, uid,ids, context=None):
        alerte=self.send_project_alert_upcomming_tasks_to_users(cr, uid, 2, context=context)
        if alerte:
            ir_model_data = self.pool.get('ir.model.data')
            try:
                compose_form_id = ir_model_data.get_object_reference(cr, uid, 'maintenance', 'maintenance_alerte_form_view')[1]
            except ValueError:
                compose_form_id = False
            res=  {
                'name':'Alert',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'maintenance.alerte',
                'views': [(compose_form_id, 'form')],
                'view_id': compose_form_id,
                'target': 'new',
                'context':{'default_name':alerte}
            }
            return res
        
    def send_project_alert_upcomming_tasks_to_users(self, cr, uid, upcoming_days, context=None):
        task_obj = self.pool.get('maintenance.task')
        
        task_ids = task_obj.search(cr, 1, [ ('state', '!=', 'done')], order='date', context=context)
        base_url = self.pool.get('ir.config_parameter').get_param(cr, 1, 'web.base.url')
        action_id = self.pool.get('ir.model.data').get_object_reference(cr, 1, 'maintenance', 'action_maintenance_task_tree')[1]
        subject = _('Tasks coming due')
        body = ''
        alerte=''
        for task in task_obj.browse(cr, 1, task_ids, context=context):
            
            if not task.date:
                continue
            
            now = datetime.now()
            date = datetime.strptime(task.date, "%Y-%m-%d")
            now_plus_upcoming_days = now + timedelta(days=upcoming_days)
            
            if date <= now:
                alerte+=_("Overdue tasks: - %s for %s \n" )% (task.name, task.repairer_id.name)
                body += '<ul>'
                body += _('<li>Overdue tasks</li>')
                body += '<ul>'
                body += '<li>'
                
                body += '<a href="' + base_url + '/#action=' + str(action_id) + '&id=' + str(task.id) + '&view_type=form">'
                body += task.name
                body += '</a>'
                
                if task.maintenance_id:
                    body += ' - ' + task.maintenance_id.name
                body += ' - ' + datetime.strptime(task.date, "%Y-%m-%d").strftime("%Y/%m/%d")
                body += '</li>'
                
                body += '</ul>'
                body += '</ul>'
            elif now < date and date <= now_plus_upcoming_days:
                alerte+=_("Tasks due: - %s for %s \n" ) % (task.name, task.repairer_id.name)
                body += '<ul>'
                body += _('<li>Tasks due in ' )+ str(upcoming_days) + _(' days</li>')
                body += '<ul>'
                body += '<li>'
                
                body += '<a href="' + base_url + '/#action=' + str(action_id) + '&id=' + str(task.id) + '&view_type=form">'
                body += task.name
                body += '</a>'
                
                if task.maintenance_id:
                    body += ' - ' + task.maintenance_id.name
                body += ' - ' + datetime.strptime(task.date, "%Y-%m-%d").strftime("%Y/%m/%d")
                body += '</li>'
                
                body += '</ul>'
                body += '</ul>'
            post_values = {
                'subject': subject,
                'body': body,
                'model':'maintenance.task',
                'partner_ids': [task.repairer_id.user_id.partner_id.id,task.maintenance_id.repairer_id.user_id.partner_id.id]}
    
            subtype = 'mail.mt_comment'
            
            thread_obj = self.pool.get('mail.thread')
            thread_obj.message_post(cr, 1, [0], type='comment', subtype=subtype, context=context, **post_values)
            
        return alerte


