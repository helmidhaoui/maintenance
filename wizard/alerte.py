# -*- coding: utf-8 -*-

from openerp import tools
from openerp.osv import fields,osv


class maintenance_alerte(osv.TransientModel):
    _name = 'maintenance.alerte'

    _columns = {
        'name':fields.text('N°',readonly=True),
    }
    
    def ok(self,cr,uid,ids,context=None):
        return True